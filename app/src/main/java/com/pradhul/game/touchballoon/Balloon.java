package com.pradhul.game.touchballoon;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;

import com.pradhul.game.touchball.R;

import java.util.Random;

public class Balloon{
    private static final int BALLOON_SPEED = 10;
    private int y = 0;
    private int x = 0;
    private int speed = 1;
    private GameView gameView;
    private Bitmap balloon;
    public Bitmap[] normalBalloons;
    private int balloonIndex = 0;

    private int normalImages[] = {R.drawable.normal_01,R.drawable.normal_02,R.drawable.normal_03,
            R.drawable.normal_04,R.drawable.normal_05,R.drawable.normal_06,R.drawable.normal_07,
            R.drawable.normal_08,
    };
    private int crackingImages[] = {R.drawable.crack_01,R.drawable.crack_02,R.drawable.crack_03,
            R.drawable.crack_04, R.drawable.crack_05,R.drawable.crack_04,R.drawable.crack_03,
            R.drawable.crack_02
    };
    private boolean reverseSwap = false;


    public Balloon(GameView gameView){
        this.gameView = gameView;
        normalBalloons = new Bitmap[8];
        setUpImages();
    }

    public  void onDraw(Canvas canvas){
        /*draws the balloon in canvas */
        animateBalloon();
        update(canvas.getWidth());
        canvas.drawBitmap(balloon, x, y, null);
    }

    public boolean isCollision(float x2, float y2) {
        return x2 > x && x2 < x + balloon.getWidth() && y2 > y && y2 < y + balloon.getHeight();
    }

    private int getRandomX(int maxVal) {
        Random rand = new Random();
        return rand.nextInt(maxVal);
    }

    private void animateBalloon() {
        /*Animates the balloon by swapping resource image at each call*/
        this.balloon = getBalloons();
        Log.d("Balloon",balloonIndex % normalBalloons.length + "");
    }

    private void update(int canvasWidth) {
        /*updates the y position for moving the balloon*/
        if (y <= 0){
            /*so that the balloon starts from bottom
            * gameView will return a height only after the View is ready
            * getting 0 in constructor of this class*/
            y = gameView.getHeight();
            /*x is assigned a random between the width od the canvas
            * so that the balloons will appear random positions from below*/
            x = getRandomX(canvasWidth - balloon.getWidth());
        }
        if (y > gameView.getHeight() - balloon.getHeight() - speed) {
            speed = -BALLOON_SPEED;
        }
        y = y + speed;
        Log.d("Balloon","Positions:"+x+","+y);
    }

    private Bitmap getBalloons() {
        if(balloonIndex == normalBalloons.length-1) {
            reverseSwap = true;
        }
        if(balloonIndex == 0){
            reverseSwap = false;
        }
        balloonIndex = reverseSwap?balloonIndex-1:balloonIndex+1;
        return normalBalloons[balloonIndex];
    }

    private void setUpImages() {
        /*setting up resources array*/
        for(int count =0; count < normalImages.length; count++){
            Bitmap balloon = BitmapFactory.decodeResource(gameView.getResources(), normalImages[count]);
            normalBalloons[count] = balloon;
        }
    }
}
