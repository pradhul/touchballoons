package com.pradhul.game.touchballoon;

import android.annotation.SuppressLint;
import android.graphics.Canvas;

public class GameLoopThread extends Thread {

    private GameView view;
    private boolean running = false;

    public GameLoopThread(GameView view){
        this.view = view;
    }
    public void setRunning(boolean run){
        running = run;
    }

    @SuppressLint("WrongCall")
    public void run(){
        while (running){
            Canvas canvas = null;
            try{
                canvas = view.getHolder().lockCanvas();
                synchronized (view.getHolder()){
                    view.onDraw(canvas);
                }
            }finally{
                if(canvas != null) {
                    view.getHolder().unlockCanvasAndPost(canvas);
                }
            }
        }

    }
}
