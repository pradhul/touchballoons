package com.pradhul.game.touchballoon;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameView extends SurfaceView implements SurfaceHolder.Callback{
    private static final int NUM_OF_BALLOONS = 7;
    /*use SurfaceView because we want complete control over the screen.
        * unlike extending View class the oDraw() Method will not be called automatically
        * from the method onSurfaceCreated() we have to call it Manually and pass a canvas object into it
        * */
    private final SurfaceHolder holder;
    private GameLoopThread gameLoopThread;
    private List<Balloon> balloons =  new ArrayList<>();

    public GameView(Context context) {
        super(context);
        gameLoopThread = new GameLoopThread(this);
        holder = getHolder();
        holder.addCallback(this);
        createBalloons(NUM_OF_BALLOONS);
    }
    public void removeBalloons(){
        Iterator<Balloon> balloonIterator = balloons.iterator();
        while(balloonIterator.hasNext()){
            Balloon balloon = balloonIterator.next();
//            if (!balloon.isCollision(event.getX(), event.getY())) {
            balloons.remove(0);
//            }
        }
    }

    private void createBalloons(int count) {
        for(int i=0 ; i< count ;i++){
            balloons.add(createBalloon());
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        if(canvas != null) {
            canvas.drawColor(Color.WHITE);
            for(Balloon balloon : balloons){
                try {
                    gameLoopThread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                balloon.onDraw(canvas);
            }
        }
    }

    @SuppressLint("WrongCall")
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        /*this is called when the view is created*/
        gameLoopThread.setRunning(true);
        gameLoopThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        /*pausing game Thread*/
        gameLoopThread.setRunning(false);
        while (true){
            try {
                gameLoopThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private  Balloon createBalloon(){
        return new Balloon(this);
    }


   /* @Override
    public synchronized boolean onTouch(View v, MotionEvent event) {
        Log.d("OnTouch real -", "x: " + event.getX() + ", Y: " + event.getY());
     *//*   for (int i = balloons.size()-1; i >= 0; i--) {
            Balloon balloon = balloons.get(i);
            Log.d("OnTouch collision -", !balloon.isCollision(event.getX(), event.getY())+"");
            if (!balloon.isCollision(event.getX(), event.getY())) {
                balloons.remove(0);
                break;
            }
        }*//*
        Iterator<Balloon>  balloonIterator = balloons.iterator();
        while(balloonIterator.hasNext()){
            Balloon balloon = balloonIterator.next();
//            if (!balloon.isCollision(event.getX(), event.getY())) {
            balloons.remove(0);
//            }
        }
        return true;
    }*/
}
